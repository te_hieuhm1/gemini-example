var express = require('express');
var router = express.Router();
const {GoogleGenerativeAI} = require('@google/generative-ai');

if (!process.env.API_KEY) {
    console.error('API_KEY environment variable not set')
    process.exit(1)
}
const genAI = new GoogleGenerativeAI(process.env.API_KEY)
const model = genAI.getGenerativeModel({model: "gemini-pro", generationConfig: {temperature: 0}})

/* GET users listing. */
router.post('/', async function (req, res, next) {
    try {
        const {note} = req.body
        const today = new Date()
        const dayOfTheWeek = today.getDay() === 0 ? 'Chủ nhật' : `${today.getDay() + 1}`
        const dateString = `${today.getDate()}/${today.getMonth() + 1}/${today.getFullYear()}`
        const prompt =
            `Bạn là một chuyên gia trong lĩnh vực AI cho nhiệm vụ nhận dạng các thực thể trong câu bao gồm:
        Action: Là hành động cần phải thực hiện
        Hour: Là thời gian cần thực hiện hành động
        Date: Là ngày cần thực hiện hành động
        Bạn hãy đặt mình vào người thực hiện công việc vận đơn của một đơn vị giao hàng, sau đó phân tích ghi chú được nhận và đảm bảo câu trả lời đúng format. 
        Biết rằng ngày hôm nay là ${dayOfTheWeek} ngày ${dateString} và trong cuộc hội thoại thường nói ngày mai là ngày hôm sau, ngày kia là hai ngày sau, ngày kìa là ba ngày sau, cuối tuần là thứ bảy và chủ nhật, tuần sau là tuần tiếp theo, tuần sau nữa là hai tuần tiếp theo.
        Câu trả lời dưới dạng json schema với các trường thông tin được định nghĩa như sau: 
        {
        "action": string //hành động của người vận đơn
         "hour": string  //giờ thực hiện hành động dưới dạng HH:MM:SS
        "date": string //ngày thực hiện hành động dưới dạng DD:MM:YYYY
        }
        
        Nếu các trường không có giá trị thì sẽ trả về null. Sau đây là một vài ví dụ:
        Hôm nay là thứ 7 ngày 23/03/2024
        input: Mai gửi giúp e Tặng lịch cấp 2
        output: {"action": "gửi", "hour": null, "date": 24/03/2024}
        input: ngày 13/3 đi đơn giúp em ạ, tặng áo 36 kg
        output: {"action":"gửi", "hour": null,"date": 13/03/2024}
        input: vc 200k, giữ đơn đến 18/3 gửi giúp em ạ
        output: {"action":"gửi", "hour": null, "date": 18/03/2024}
        input: PH hẹn thứ 3 nhận thẻ học
        output: {"action":"gửi", "hour": null, "date": 26/03/2024}
        input: TẶNG PACKAGE 2018 CŨ
        output: {"action":"tặng", "hour": null,"date": null}
        input: cho xem thẻ hướng dẫn vào lớp ( voucher 400k )
        output: {"action": "xem", "hour": null,"date": null}
        input: tools-cb13-lớp 3
        output: {"action": null, "hour": null,"date": null}
        
        Tương tự như những ví dụ trên hãy phân tích câu sau:
        input: ${note}
        output:
        `
        const result = await model.generateContent(prompt)
        const response = await result.response
        res.send(response.text());
    } catch (e) {
        res.status(500).send(e.message)
    }

});

module.exports = router;
